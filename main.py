import asyncio
import logging
from common.messaging.vumos.vumos import VumosMessageType, VumosService, VumosServiceStatus, VumosMessage
import json
import re
import persistqueue
import subprocess
from threading import Thread
import atomics
from urllib.parse import urlparse
import socket

from common.messaging.vumos import ScheduledVumosService

# this is to make it possible to delete this queue from inside the task, which will create it again
# this may cause concurrency issues, need more testing, but should only be used when restarting the task from the UI
queueHolder = {}
queueHolder['queue'] = persistqueue.UniqueQ('persistent/queue.db', multithreading=True, auto_commit=False)

atomic_scanned_links = atomics.atomic(width=4, atype=atomics.INT)
atomic_vulnerabilities = atomics.atomic(width=4, atype=atomics.INT)
atomic_scanning = atomics.atomic(width=4, atype=atomics.INT)

event_loop = asyncio.get_event_loop()

sqlmap_level = 3
sqlmap_techniques = 'BEUSTQ'

# TODO: fix multi thread restarting behaviour
async def task(service: ScheduledVumosService, _: None = None):
    global sqlmap_level 
    global sqlmap_techniques

    del queueHolder['queue']
    queueHolder['queue'] = persistqueue.UniqueQ('persistent/queue.db', multithreading=True, auto_commit=False)
    num_worker_threads = int(service.get_config("num_threads"))
    sqlmap_level = int(service.get_config("sqlmap_level"))
    sqlmap_techniques = service.get_config("sqlmap_techniques")
    logging.info(f"Starting sqlmap task with {num_worker_threads} threads, level {sqlmap_level} and {sqlmap_techniques}")
    
    coros = []
    for i in range(num_worker_threads):
        coros.append(asyncio.get_event_loop().run_in_executor(None, worker))
    await asyncio.gather(*coros)

    logging.warning("all worker threads exited!")

def worker():
    logging.info("Starting new sqlmap worker")
    while True:

        url = queueHolder['queue'].get() # this should block until there is new stuff in the queue
        if not url:
            break
        scanned_links = atomic_scanned_links.load()
        vulnerabilities = atomic_vulnerabilities.load()

        status = f"[RUNNING] Scanning {url['url']}. ({vulnerabilities}/{scanned_links} vulnerable) ({queueHolder['queue'].size} in queue)"
        logging.info(status)
        service.set_status(VumosServiceStatus("running", status))
        run_sqlmap(url)
        queueHolder['queue'].task_done()
    logging.warn("Queue is empty")


# response =  {'parent_url': 'https://wiki.imesec.ime.usp.br/register', 'urls': [{'method': 'POST', 'domain': 'wiki.imesec.ime.usp.br', 'vars': '_token=eWxxP&name=a&email=a&password=a&_token=eWxxP&name=a&email=a&password=a', 'type': 'form', 'url': 'https://wiki.imesec.ime.usp.br/register', 'sqlmap': "--method=POST -u 'https://wiki.imesec.ime.usp.br/register' --data='_token=eWxxP&name=a&email=a&password=a'"}], 'ip': '143.107.44.127', 'domain': 'wiki.imesec.ime.usp.br'}

def run_sqlmap(url_data):
    logging.info(f"Running sqlmap on {url_data['url']}...")
    sqlmap_command = 'sqlmap ' + url_data['sqlmap']
    
    sqlmap_command += ' --threads=1'
    sqlmap_command += f' --level={str(sqlmap_level)}'
    # sqlmap_command += ' --smart') # no, smart is actually pretty dumb
    sqlmap_command += f' --technique={sqlmap_techniques}'
    sqlmap_command += ' --batch'
    sqlmap_command += ' --disable-coloring'

    sqlmap_process = subprocess.run(sqlmap_command, stdout=asyncio.subprocess.PIPE, shell=True)

    command_output = sqlmap_process.stdout
    logging.info(command_output)
    atomic_scanned_links.inc()
    
    if b'sqlmap identified the following injection point' not in command_output and b'sqlmap resumed the following injection point(s) from stored session' not in command_output:
        logging.info(f"Finished scanning and no vulnerability was found for {url_data['url']}")
        # TODO: ?
    else:
        injectable_str = command_output.split(b'\n---\n')[1].decode()
        parameter_reg = re.search(r'(\s*Parameter: )(.+)( \((.+)\))', injectable_str.split('\n')[0])
        extra = {
            "parameter": parameter_reg[2],
            "method": parameter_reg[4],
            "techniques": [],
            "parent_url": url_data['parent_url'],
            "url_type": url_data['type'],
            "url": url_data['url'],
        }
        techniques_str = injectable_str.split('\n\n')
        for technique in techniques_str:
            extra['techniques'].append({
                "type": re.search(r'(\s+Type: )(.+)', technique)[2],
                "title": re.search(r'(\s+Title: )(.+)', technique)[2],
                "payload": re.search(r'(\s+Payload: )(.+)', technique)[2],
            })
        description = f"Found SQL Injection on parameter {extra['parameter']} ({extra['method']}), which is a {url_data['type']} on page {url_data['parent_url']}"
        logging.info(f"Found SQL Injection on {url_data['url']} on {extra['parameter']} ({extra['method']}), which is a {url_data['type']} on page {url_data['parent_url']}")
        atomic_vulnerabilities.inc()
        logging.info(extra)
        future = asyncio.run_coroutine_threadsafe(service.send_issue_data(url_data['ip'], url_data['port'], "SQL Injection", fixed=False, description=description, extra=extra), event_loop)
        future.result()


# Initialize Vumos service
service = ScheduledVumosService(
    "SQLMap Runner",
    "Listens for messages that contain a dynamic URL (either a form or a link) and runs sqlmap on them",
    conditions=lambda s: True,
    task=task,
    parameters=[
        {
            "name": "Number of instances",
            "description": "Amount of instances of SQLMap to run simultaneously ",
            "key": "num_threads",
            "value": {
                "type": "integer",
                "default": "1"
            }
        },
        {
            "name": "SQLMap Techniques",
            "description": "SQLMap techniques to use",
            "key": "sqlmap_techniques",
            "value": {
                "type": "string",
                "default": "BEUSTQ"
            }
        },
        {
            "name": "SQLMap Level",
            "description": "SQLMap level to use",
            "key": "sqlmap_level",
            "value": {
                "type": "integer",
                "default": "3"
            }
        },
    ],
    pool_interval=5 # Runs task 5 secods
)

logging.getLogger().setLevel(logging.INFO)

async def path_handler(service: VumosService, message: VumosMessage):
    logging.info(f"enqueuing URLs for {message['data']['parent_url']}")
    for url in message['data']['urls']:
        url['parent_url'] = message['data']['parent_url']
        url['ip'] = socket.gethostbyname(url["domain"])
        parsed_url = urlparse(url['url'])
        url['port'] = 80 if parsed_url.scheme == 'http' or parsed_url.scheme == None else 443
        url['port'] = parsed_url.port if parsed_url.port else url['port']

        logging.debug(f"enqueuing URL: {url['url']}")
        queueHolder['queue'].put(url)
service.on(filter=lambda m : m['message'] == 'data_path', handler=path_handler)

logging.info("starting sqlmap runner module")
asyncio.get_event_loop().run_until_complete(service.connect())
service.loop()
