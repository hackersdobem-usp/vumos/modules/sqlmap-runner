FROM python:3.9

RUN apt-get update && apt-get install sqlmap -y

COPY Pipfile Pipfile.lock /usr/src/app/

WORKDIR /usr/src/app

RUN pip install pipenv

RUN pipenv install --system --deploy

COPY . .

VOLUME [ "/usr/src/app/persistent" ]
CMD [ "python", "-u", "main.py" ]
